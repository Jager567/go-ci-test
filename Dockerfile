FROM bitnami/minideb:stretch

ENV GOPATH /go
ARG GO_VER=.*?

RUN set -eux; \
	\
	# Basic requirements
	install_packages \
		ca-certificates \
		wget \
		git \
		; \
	\
	# Install dep
	mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"; \
	DEP_VERSION="$(wget -qO- "https://api.github.com/repos/golang/dep/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")')"; \
	wget https://github.com/golang/dep/releases/download/${DEP_VERSION}/dep-linux-amd64 -O $GOPATH/bin/dep --progress=dot:mega; \
	chmod +x $GOPATH/bin/dep; \
	\
	##################################
	#                                #
	#             CLEANUP            #
	#                                #
	##################################
	\
	apt-get --purge autoremove wget -y; \
	apt-get clean; \
	rm -rf /tmp/* \
		/var/tmp/* \
		/usr/share/doc/* \
		/usr/share/groff/* \
		/usr/share/info/* \
		/usr/share/linda/* \
		/usr/share/lintian/* \
		/usr/share/locale/* \
		/usr/share/man/*

RUN set -eux; \
	##################################
	#                                #
	#         INSTALL GOLANG         #
	#                                #
	##################################
	\
	install_packages wget; \
	perl -e ' \
		my $html = `wget -qO- golang.org/dl/`; \
		my $arch = `dpkg --print-architecture`; \
		chomp($arch); \
		my $ver = $ENV{'GO_VER'}; \
		print "$arch\n$ver\n"; \
		if ($html =~ m/(https:\/\/dl\.google\.com\/go\/go${ver}\.linux-${arch}\.tar\.gz)(?:.*?\n){4}.*?\n.*?([A-Fa-f0-9]{64})/) { \
			print "$1 | $2\n"; \
			system("wget \"${1}\" -O go.tgz --progress=dot:giga"); \
			system("echo \"${2} *go.tgz\" | sha256sum -c -"); \
		} else { \
			print "Did not match\n"; \
			exit(1); \
		} \
	'; \
	rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi; \
	tar -C /usr/local -xzf go.tgz; \
	\
	export PATH="/usr/local/go/bin:$PATH"; \
	go version; \
	\
	##################################
	#                                #
	#             CLEANUP            #
	#                                #
	##################################
	\
	apt-get --purge autoremove wget -y; \
	apt-get clean; \
	rm -rf /tmp/* \
		/var/tmp/* \
		/usr/share/doc/* \
		/usr/share/groff/* \
		/usr/share/info/* \
		/usr/share/linda/* \
		/usr/share/lintian/* \
		/usr/share/locale/* \
		/usr/share/man/* \
		go.tgz

ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

WORKDIR $GOPATH
